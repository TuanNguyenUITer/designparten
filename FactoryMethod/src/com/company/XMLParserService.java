package com.company;

public abstract class XMLParserService {
    public void display(){
        XMLParser parser = getParser();
        parser.parse();
    }

    protected abstract XMLParser getParser();

}
