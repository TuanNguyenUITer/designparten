package com.company;

public class ResponseXMLDisplayService extends XMLParserService{

	@Override
	protected XMLParser getParser() {
		return new ResponseXMLParser();
	}

}
