package com.company;

public class Client {
    public static void main(String[] args) {
        XMLParserService service = new ResponseXMLDisplayService();
        service.display();

        service = new ErrorXMLDisplayService();
        service.display();

    }
}
