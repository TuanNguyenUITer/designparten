package com.company;

public interface IMilkTea {
	double cost();
	int salary();
}
