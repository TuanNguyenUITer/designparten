package com.company;

public abstract class MilkTeaDecorator implements IMilkTea{
	IMilkTea milkTea;

	public MilkTeaDecorator(IMilkTea wrappee)
	{
		milkTea=wrappee;
	}

	@Override
	public double cost() {
		return milkTea.cost();
	}

	@Override
	public int salary() {
		return milkTea.salary();
	}

}
