package com.company;

public class WhiteBubble extends MilkTeaDecorator{

	public WhiteBubble(IMilkTea inner) {
		super(inner);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double cost() {
		// TODO Auto-generated method stub
		return 6000+milkTea.cost();
	}

	@Override
	public int salary() {
		return 4000+milkTea.salary();
	}
}