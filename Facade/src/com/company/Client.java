package com.company;



public class Client {
	public static void main(String[] args) {
		BankFacade.getInstance().clientCheckBalance();
		
		BankFacade.getInstance().clientSendMoney();
		
		BankFacade.getInstance().clientWithdrawMoney();
	}
}
