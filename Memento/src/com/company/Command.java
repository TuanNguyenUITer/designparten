package com.company;

public interface Command {
    String getName();
    void execute();
}
