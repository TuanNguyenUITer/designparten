﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iterator
{
    interface IAbstractCollection
    {
        Iterator CreateIterator();
    }
}
