package com.company;

public class SeparatorExpression implements AbstractExpression{

	@Override
	public void Evaluate(Context context) {
		String expression=context.expression;
		context.expression = expression.replace(" ", "-");
	}

}
