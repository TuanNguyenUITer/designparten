package com.company;

public interface AbstractExpression
{
    void Evaluate(Context context);
}