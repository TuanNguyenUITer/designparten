package com.company.PlasticFactory;


import com.company.AbstractProduct.Chair;
import com.company.AbstractProduct.Table;
import com.company.FurnitureAbstrastFactory.FurnitureAbstractFactory;

public class PlasticFactory extends FurnitureAbstractFactory {
	 
    @Override
    public Chair createChair() {
        return new PlasticChair();
    }
 
    @Override
    public Table createTable() {
        return new PlasticTable();
    }
 
}