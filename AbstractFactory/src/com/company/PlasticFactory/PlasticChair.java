package com.company.PlasticFactory;


import com.company.AbstractProduct.Chair;

public class PlasticChair implements Chair {
    @Override
    public void create() {
        System.out.println("Create plastic chair");
    }
}
