package com.company.PlasticFactory;


import com.company.AbstractProduct.Table;

public class PlasticTable implements Table {
    @Override
    public void create() {
        System.out.println("Create plastic table");
    }
}

