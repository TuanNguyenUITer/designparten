package com.company.WoodFactory;


import com.company.AbstractProduct.Chair;
import com.company.AbstractProduct.Table;
import com.company.FurnitureAbstrastFactory.FurnitureAbstractFactory;

public class WoodFactory extends FurnitureAbstractFactory {
	 
    @Override
    public Chair createChair() {
        return new WoodChair();
    }
 
    @Override
    public Table createTable() {
        return new WoodTable();
    }
}