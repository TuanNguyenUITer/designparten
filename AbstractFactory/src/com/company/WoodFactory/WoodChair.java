package com.company.WoodFactory;


import com.company.AbstractProduct.Chair;

public class WoodChair implements Chair {
    @Override
    public void create() {
        System.out.println("Create wood chair");
    }
}
