package com.company.FurnitureAbstrastFactory;


import com.company.AbstractProduct.Chair;
import com.company.AbstractProduct.Table;

public abstract class FurnitureAbstractFactory {
	 public abstract Chair createChair();
	 
	 public abstract Table createTable();
}
