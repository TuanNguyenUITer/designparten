package com.company;

public interface Prototype {
	Prototype createClone();
}
