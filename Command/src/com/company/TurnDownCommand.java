package com.company;

public class TurnDownCommand implements Command {

	TV theDevice;
	
	public TurnDownCommand(TV newDevice){
		
		theDevice = newDevice;
		
	}
	
	public void execute() {
		
		theDevice.volumeUp();
		
	}

	public void undo() {
		
		theDevice.volumenDown();
		
	}
	
}