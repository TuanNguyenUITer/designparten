package com.company;

public class TurnOffCommand implements Command {

	TV theDevice;
	
	public TurnOffCommand(TV newDevice){
		
		theDevice = newDevice;
		
	}
	
	public void execute() {
		
		theDevice.off();
		
	}

	
	
	public void undo() {
		
		theDevice.on();
		
	}
	
}